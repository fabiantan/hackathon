<?php
include ('./streamapi.php');
include ('./config.php');

extract($config);
$keyword = array (
		'#aws',
		'#awscloud',
		'#mh370',
);
//https://dev.twitter.com/docs/streaming-apis/connecting
//https://dev.twitter.com/docs/api/1.1/post/statuses/filter
//https://dev.twitter.com/docs/streaming-apis/parameters#track

$t = new twitter_stream();
$t->login($consumerkey, $consumersecret, $accesstoken, $accesstokensecret);
$t->start_streaming($keyword);
